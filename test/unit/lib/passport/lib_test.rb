require "test_helper"

class LibTest < ActiveSupport::TestCase
  test "the truth" do
    assert true
  end

  test "connection" do 
    Passport.k = "89cb1ba95d21fa7f1a48b26675153d7c"
    Passport.endpoint = "http://localhost:3000/api"
    assert Passport::Lib.valid?, "Chave inválida"
  end

  test "get current user" do 
    Passport.k = "89cb1ba95d21fa7f1a48b26675153d7c"
    Passport.endpoint = "http://localhost:3000/api"
    assert Passport::Lib.current_user['id'] == 1
  end

  test "get current user email" do 
    Passport.k = "89cb1ba95d21fa7f1a48b26675153d7c"
    Passport.endpoint = "http://localhost:3000/api"
    assert Passport::Lib.current_user['email'] == "marcelo@tracersoft.com.br"
  end

  test "get user" do 
    Passport.k = "89cb1ba95d21fa7f1a48b26675153d7c"
    Passport.endpoint = "http://localhost:3000/api"
    user = Passport::Lib.current_user
    assert Passport::Lib.user(user['id']) == user['id']
  end
end