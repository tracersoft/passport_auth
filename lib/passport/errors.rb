module Passport
	class Error < StandardError
		def initialize(msg)
			@msg = msg
		end

		def to_s
		  "#{self.class.to_s}: #{@msg}"
		end
	end

  class APIError < Error
    attr_accessor :error

    def initialize(error)
      @error = error
    end

    def to_s
      "#{self.class.to_s}: #{@error}"
    end
  end
end