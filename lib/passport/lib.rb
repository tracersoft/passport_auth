module Passport
	class Lib
		class << self
			def valid?
				begin
					request "validate_key"
				rescue Passport::APIError
					false
				end
			end

			def current_user
				request "users/current_user"
			end

			def user(id)
				request "users/#{id}"
			end

			def users
				request "users"
			end

			def role(id)
				request "users/roles/#{id}"
			end

			def roles
				request "users/roles"
			end

			def current_role
				role current_user.id
			end

			def parse_response(res)
				begin
						MultiJson.load res
				rescue MultiJson::LoadError => e
						raise Passport::Error.new("Server response is not a valid JSON: #{res}")
				end
			end

			def request(req, method=:get, payload=nil)
				unless Passport.endpoint
					raise Passport::Error.new("Configure an endpoint before request something")
				end
				unless Passport.k
					raise Passport::Error.new("Configure an API key before request something")
				end
				
				api_uri = Passport.api_uri req
				params = { :k=>Passport.k }

				begin
					res = RestClient::Request.execute({
						:method=>method,
						:url=>api_uri,
						:payload=>params
					})

				rescue SocketError => e
					raise Passport::Error.new "Error connecting to server (#{e.message})."
				rescue NoMethodError => e
					if e.message =~ /\WRequestFailed\W/
					  raise ResponseError.new("Unexpected response code (#{e.inspect}).")
					else
					  raise
					end
				  
				rescue RestClient::ExceptionWithResponse => e
				  error = parse_response e.http_body
				  raise Passport::APIError.new error
				
				rescue RestClient::Exception, Errno::ECONNREFUSED => e
					raise Passport::Error.new "Error connecting to server: connection refused"	
				end

				parse_response res
			end
		end
	end
end