module PassportAuth
  class Engine < ::Rails::Engine
    class Config
      @endpoint = nil
      class << self
        attr_accessor :endpoint, :root_path
      end
    end
    isolate_namespace PassportAuth
  end
end
