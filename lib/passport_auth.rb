require "passport_auth/engine"
require "ext_controller"
require "passport"

module PassportAuth
	@@fake_login = false

	def self.fake_login
		@@fake_login
	end

	def self.fake_login=(v)
		@@fake_login = v
	end
end
