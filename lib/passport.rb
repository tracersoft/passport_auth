require "passport/lib"
require "passport/errors"
require "rest-client"

module Passport
	@@k = nil
	@@endpoint = nil

	def self.k=(k)
		@@k = k
	end

	def self.k
		@@k
	end

	def self.endpoint=(endpoint)
		@@endpoint = endpoint
	end

	def self.endpoint
		@@endpoint
	end

	def self.api_uri(req)
		"#{@@endpoint}/#{req}"
	end
end