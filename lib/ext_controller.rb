module ActionController
	class Base < Metal
		def try_authentication
			if params[:k]
				Passport.k = params[:k]
				if Passport::Lib.valid?
					session[:k] = params[:k]
					session[:logged] = true
				end
			elsif Passport.k && Passport::Lib.valid?
				session[:k] = params[:k]
				session[:logged] = true				
			end
		end

		def current_key
			try_authentication
			unless PassportAuth.fake_login
				return session[:k]	
			end
			Passport.k
		end

		def user_signed_in?
			try_authentication
			session[:logged] == true or PassportAuth.fake_login
		end

		def authenticate_user!
			try_authentication
			if session[:logged] or PassportAuth.fake_login
				true
			else
				render :text=>"Unauthorized", :status=>:unauthorized
				false
			end
		end
	end
end
