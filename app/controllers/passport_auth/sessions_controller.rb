require_dependency "passport_auth/application_controller"

module PassportAuth
  class SessionsController < ApplicationController
  	def new
      Passport.k = params[:k]
      Passport.endpoint = PassportAuth::Engine::Config.endpoint
  		if Passport::Lib.valid?
  			session[:k] = params[:k]
        session[:logged] = true
        if params[:b]
          redirect_to params[:b]
        else
          redirect_to PassportAuth::Engine::Config.root_path
  		  end
      else
        session[:logged] = false
  			head :unauthorized
  		end
  	end
  end
end
