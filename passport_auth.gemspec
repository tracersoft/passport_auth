$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "passport_auth/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "passport_auth"
  s.version     = PassportAuth::VERSION
  s.authors     = ["Marcelo Piva"]
  s.email       = ["marcelo@tracersoft.com.br"]
  s.homepage    = "https://bitbucket.org/tracersoft/passport_auth"
  s.summary     = "Rails Engine for Passport authetication."

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.0.4"

  s.add_development_dependency "sqlite3"
  s.add_dependency "rest-client"
  s.add_dependency "multi_json"
end
